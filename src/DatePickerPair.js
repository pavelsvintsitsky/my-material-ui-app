import React, {Component} from 'react';
import DatePicker from 'material-ui/DatePicker';

class DatePickerPair extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstDate: null,
            secondDate: null
        };
        this.secondDatePicker = null;
    }

    handleFirstDateChange = (event, date) => {
        this.setState({
            firstDate: date,
            secondDate: null
        });
    };

    formatDate = (date, options = {weekday: 'short', month: 'short', day: 'numeric'}) => {
        if (!date) return '';

        return date.toLocaleDateString('en-US', options);
    };

    formatAndReplace = (date) => {
        if (!date) return;

        const formattedDate = this.formatDate(date)
        const monthName = this.formatDate(date, {month: 'long'})

        const elements = Array.from(document.querySelectorAll('*')).filter(element => element.textContent.includes(formattedDate));

        elements.forEach(element => {
            if (element.textContent === formattedDate) {
                element.classList.add('date-text-for-replace')
                element.setAttribute('data-after', monthName)
            }
        });
    };

    handleSecondDateFocus = () => {
        const {firstDate} = this.state;
        if (firstDate && this.secondDatePicker) {
            setTimeout(() => {
                const dialogWindow = this.secondDatePicker.refs.dialogWindow;
                if (dialogWindow) {
                    const displayDate = new Date(firstDate.getFullYear(), firstDate.getMonth(), 1);

                    // TODO Add functionality to change month (change the selected date so user does not have the possibility to see the chosen date)
                    const selectedDate = new Date(firstDate.getFullYear(), firstDate.getMonth() - 1, 1);

                    dialogWindow.refs.calendar.setState({
                        displayDate,
                        selectedDate,
                    });

                    this.formatAndReplace(selectedDate);
                }
            }, 300);
        }
    };

    handleSecondDateChange = (event, date) => {
        this.setState({secondDate: date});
    };

    render() {
        const {firstDate, secondDate} = this.state;

        return (

            <div>
                <DatePicker
                    hintText="Please select first date"
                    value={firstDate}
                    onChange={this.handleFirstDateChange}
                />
                <DatePicker
                    hintText="Please select second date"
                    ref={(ref) => (this.secondDatePicker = ref)}
                    value={secondDate}
                    onFocus={this.handleSecondDateFocus}
                    onChange={this.handleSecondDateChange}
                />
            </div>

        );
    }
}

export default DatePickerPair;
