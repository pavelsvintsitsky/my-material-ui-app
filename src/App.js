import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DatePickerPair from "./DatePickerPair";

import './App.css'

const App = () => (
    <MuiThemeProvider>
        <div className="main">
            <h1>Welcome to Material-UI 0.17.2, ENJOY!</h1>
            <DatePickerPair />
        </div>
    </MuiThemeProvider>
);

export default App
